package AIF;

import AIF.AerialVehicles.UAVs.Haron.Eitan;
import AIF.Missions.Mission;
import AIF.Missions.MissionTypeException;
import org.junit.Before;
import org.junit.Test;

public class EitanTest {
    private Eitan eitan;
    private AIFUtil aifUtil;

    @Before
    public void beforeTest() {
        this.aifUtil = new AIFUtil();
        this.eitan = (Eitan) aifUtil.getAerialVehiclesHashMap().get("Etan");
    }

    @Test
    public void setMissionTest_executableMission() throws MissionTypeException {
        Mission mission = aifUtil.getAllMissions().get("attack");
        this.eitan.setMission(mission);
    }

    @Test(expected = MissionTypeException.class)
    public void setMissionTest_unexecutableMission() throws MissionTypeException {
        Mission mission = aifUtil.getAllMissions().get("bda");
        this.eitan.setMission(mission);
    }
}
