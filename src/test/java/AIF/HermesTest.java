package AIF;

import AIF.AerialVehicles.UAVs.Hermes.Kochav;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;


public class HermesTest {

    public static AIFUtil aifUtil;
    public static Kochav kochav;

    @BeforeClass
    public static void setUpBeforeClass() {
        aifUtil = new AIFUtil();
        kochav = (Kochav) aifUtil.getAerialVehiclesHashMap().get("Kohav");
    }

    @BeforeClass
    public static void beforeAll(){
        aifUtil = new AIFUtil();
    }

    @Test
    public void testCheckHermesEquilibriumGroupsInGroup(){
        aifUtil.setHoursAndCheck(kochav,78);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 84 shouldn't be reset to 0 after Kohav.check().", kochav.getHoursOfFlightSinceLastRepair() == 78);
    }

    @Test
    public void testCheckHermesEquilibriumGroupsAboveGroup(){
        aifUtil.setHoursAndCheck(kochav,123);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 0 should  0 Kohav.check().", kochav.getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsFighterJetsLowerBoundMiddle(){
        aifUtil.setHoursAndCheck(kochav,0);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 250 should be reset to 0 Kohav.check().", kochav.getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsFighterJetsLowerBoundLeft(){
        aifUtil.setHoursAndCheck(kochav,-1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = -1 shouldn't be reset to 0 Kohav.check().", kochav.getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsFighterJetsLowerBoundRight(){
        aifUtil.setHoursAndCheck(kochav,1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 250 should be reset to 0 Kohav.check().", kochav.getHoursOfFlightSinceLastRepair() == 1);
    }

    @Test
    public void testCheckLimitsFighterJetsUpperBoundRight(){
        aifUtil.setHoursAndCheck(kochav,101);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 101 should be reset to 0 Kohav.check().", kochav.getHoursOfFlightSinceLastRepair() == 0);
    }
}
