package AIF;

import AIF.AerialVehicles.AerialVehicle;
import AIF.Missions.BdaMission;
import AIF.Missions.Mission;
import AIF.Missions.MissionTypeException;
import org.junit.Before;
import org.junit.Test;

public class Zik {
    private AerialVehicle zik;
    private AIFUtil aifUtil;

    @Before
    public void beforeTest() {
        this.aifUtil = new AIFUtil();
        this.zik = this.aifUtil.getAerialVehiclesHashMap().get("Zik");
    }

    @Test
    public void setMissionTest_executableMission() throws MissionTypeException {
        Mission mission = aifUtil.getAllMissions().get("bda");
        this.zik.setMission(mission);
    }

    @Test(expected = MissionTypeException.class)
    public void setMissionTest_unexecutableMission() throws MissionTypeException {
        Mission mission = aifUtil.getAllMissions().get("attack");
        this.zik.setMission(mission);
    }
}
