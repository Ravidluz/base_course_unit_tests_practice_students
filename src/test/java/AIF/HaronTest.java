package AIF;

import AIF.AerialVehicles.UAVs.Haron.Eitan;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;


public class HaronTest {

    public static AIFUtil aifUtil;
    public static Eitan eitan;

    @BeforeClass
    public static void setUpBeforeClass() {
        aifUtil = new AIFUtil();
       eitan = (Eitan) aifUtil.getAerialVehiclesHashMap().get("Etan");
    }

    @BeforeClass
    public static void beforeAll() {
        aifUtil = new AIFUtil();
    }

    @Test
    public void testCheckHermesEquilibriumGroupsInGroup() {
        aifUtil.setHoursAndCheck(eitan, 78);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 84 shouldn't be reset to 0 after Etan.check().", eitan.getHoursOfFlightSinceLastRepair() == 78);
    }


    @Test
    public void testCheckHermesEquilibriumGroupsAboveGroup() {
        aifUtil.setHoursAndCheck(eitan, 151);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 0 should  0 Etan.check().", eitan.getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsFighterJetsLowerBoundMiddle() {
        aifUtil.setHoursAndCheck(eitan, 0);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 250 should be reset to 0 Etan.check().", eitan.getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsFighterJetsLowerBoundLeft() {
        aifUtil.setHoursAndCheck(eitan, -1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = -1 shouldn't be reset to 0 Etan.check().", eitan.getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsFighterJetsLowerBoundRight() {
        aifUtil.setHoursAndCheck(eitan, 1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 250 should be reset to 0 Etan.check().", eitan.getHoursOfFlightSinceLastRepair() == 1);
    }

    @Test
    public void testCheckLimitsFighterJetsUpperBoundRight() {
        aifUtil.setHoursAndCheck(eitan, 151);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 101 should be reset to 0 Etan.check().", eitan.getHoursOfFlightSinceLastRepair() == 0);
    }
}
