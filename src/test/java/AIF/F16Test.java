package AIF;

import AIF.AerialVehicles.AerialVehicle;
import AIF.AerialVehicles.FighterJets.F16;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

public class F16Test {
    private static AIFUtil aifUtil;
    public static AerialVehicle f16;

    @BeforeClass
    public static void setUpBeforeClass() {
        aifUtil = new AIFUtil();
        f16 = aifUtil.getAerialVehiclesHashMap().get("F16");
    }

    @Test
    public void setMission_executable() throws MissionTypeException {
        f16.setMission(aifUtil.getAllMissions().get("attack"));
    }

    @Test(expected = MissionTypeException.class)
    public void setMission_unexecutable() throws MissionTypeException {
        f16.setMission(aifUtil.getAllMissions().get("falafel"));
    }
}
