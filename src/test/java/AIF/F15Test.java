package AIF;

import AIF.AerialVehicles.AerialVehicle;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

public class F15Test {
    private static AIFUtil aifUtil;
    public static AerialVehicle f15;

    @BeforeClass
    public static void setUpBeforeClass() {
        aifUtil = new AIFUtil();
        f15 = aifUtil.getAerialVehiclesHashMap().get("F15");
    }

    @Test
    public void setMission_executable() throws MissionTypeException {
        f15.setMission(aifUtil.getAllMissions().get("attack"));
    }

    @Test(expected = MissionTypeException.class)
    public void setMission_unexecutable() throws MissionTypeException {
        f15.setMission(aifUtil.getAllMissions().get("meow"));
    }
}
