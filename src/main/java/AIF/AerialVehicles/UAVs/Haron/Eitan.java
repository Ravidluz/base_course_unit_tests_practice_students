package AIF.AerialVehicles.UAVs.Haron;

import AIF.Missions.*;

public class Eitan extends Haron {
    public Eitan(int amountOfMissile, String missileModel, String sensorType, String pilotName, Mission mission, int hoursOfFlightSinceLastRepair, Boolean readyToFly) {
        super(amountOfMissile, missileModel, sensorType, pilotName, mission, hoursOfFlightSinceLastRepair, readyToFly);
    }

    @Override
    public void setMission(Mission mission) throws MissionTypeException {
        if (mission instanceof IntelligenceMission || mission instanceof AttackMission) {
            this.mission = mission;
        } else {
            throw new MissionTypeException("this vehicle cannot preform this mission");
        }
    }
}
