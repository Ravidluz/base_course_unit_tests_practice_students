package AIF.AerialVehicles.UAVs.Haron;

import AIF.AerialVehicles.AerialBdaVehicle;
import AIF.Missions.BdaMission;
import AIF.Missions.Mission;
import AIF.Missions.MissionTypeException;

public class Shoval extends Haron implements AerialBdaVehicle {
    String cameraType;

    public Shoval(String cameraType, int amountOfMissile, String missileModel, String sensorType, String pilotName, Mission mission, int hoursOfFlightSinceLastRepair, Boolean readyToFly) {
        super(amountOfMissile, missileModel, sensorType, pilotName, mission, hoursOfFlightSinceLastRepair, readyToFly);
        this.cameraType = cameraType;
    }

    @Override
    public String preformBda() {
        String message = this.getPilotName() + ": " + this.getClass().getSimpleName() +
                " taking pictures of " + ((BdaMission)this.getMission()).getObjective() +
                " with: " + this.getCameraType() + " camera";
        System.out.println(message);
        return message;
    }

    @Override
    public void setMission(Mission mission) throws MissionTypeException {
        this.mission = mission;
    }


    public String getCameraType() {
        return this.cameraType;
    }
}

