package AIF.Missions;

import AIF.Entities.Coordinates;

public class AttackMission extends Mission {
    String target;
 
    public AttackMission(String target, Coordinates coordinates) {
        super(coordinates);
        this.target = target;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public String toString(){
        return "Attack Mission{"  +
               
                "Coordinates = " + super.coordinates.toString()  +
                '}'+ '\n';
    }



}
