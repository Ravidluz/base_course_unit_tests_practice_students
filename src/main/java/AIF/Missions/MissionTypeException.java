package AIF.Missions;

public class MissionTypeException extends Exception {
    public MissionTypeException(String message) {
        super(message);
    }
}
